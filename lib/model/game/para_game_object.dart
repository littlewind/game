import 'package:elearning/model/core/question.dart';
import 'package:elearning/model/game/game_object.dart';
import 'package:elearning/model/game/quiz_game_object.dart';

class ParaGameObject extends GameObject {
  List<QuizGameObject> children = [];
  List<int> childrenIndex = [];

  ParaGameObject.fromQuestion(Question questionDb)
      : super.fromQuestion(questionDb);
}
