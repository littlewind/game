import 'package:elearning/model/core/choice.dart';
import 'package:elearning/model/core/face.dart';
import 'package:elearning/model/core/question.dart';
import 'package:elearning/model/game/game_object.dart';
import 'package:elearning/screen/study/spelling_view.dart';

class SpellingGameObject extends GameObject {
  Face answer;
  String userInput = '';

  SpellingGameObject.fromQuestion(Question questionDb) : super.fromQuestion(questionDb) {
    String content = questionDb.content.replaceAll(' ', '');
    answer = Face()
      ..content = content
      ..id = questionDb.id;
    if (questionDb.choices.isNotEmpty) {
      Choice correctChoice = questionDb.choices
          .firstWhere((element) => element.isCorrect, orElse: () => null);
      question = Face()
        ..content = correctChoice.content
        ..id = questionDb.id;
    }
  }

  onAnswer(SpellAnswerParams params) {
    switch (params.type) {
      case SpellCardAnswerType.send_answer:
        if (status == GameObjectStatus.answered) {
          return;
        }
        userInput = params.answer;
        if (userInput.toLowerCase() == answer.content.toLowerCase()) {
          status = GameObjectStatus.answered;
          questionStatus = QuestionStatus.answeredCorrect;
        } else {
          status = GameObjectStatus.answered;
          questionStatus = QuestionStatus.answeredIncorrect;
        }
        break;
      case SpellCardAnswerType.user_input_value:
        userInput = params.answer;
        break;
      default:
        break;
    }
  }

  reset() {
    super.reset();
    userInput = '';
    userInput = userInput.padRight(answer.content.length, '_');
  }

}