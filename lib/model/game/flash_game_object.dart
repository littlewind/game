import 'package:elearning/model/core/face.dart';
import 'package:elearning/model/core/question.dart';
import 'package:elearning/model/game/game_object.dart';

class FlashGameObject extends GameObject {
  Face answer;
  FlashGameObject.fromQuestion(Question questionDb) : super.fromQuestion(questionDb) {
    answer = Face()..content = questionDb.choices.first.content;
  }

  onAnswer() {
    if (status == GameObjectStatus.answered) {
      return;
    } else {
      status = GameObjectStatus.answered;
      questionStatus = QuestionStatus.answeredCorrect;
    }
  }

}