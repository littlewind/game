import 'package:uuid/uuid.dart';

final String tableChoice = 'Choice';
final String _columnId = 'id';
final String _columnParentId = 'parentId';
final String _columnSelected = 'selected';
final String _columnTestId = 'testId';
final String _columnContent = 'content';
final String _columnIsCorrect = 'isCorrect';

final String createChoiceTable = '''
        create table IF NOT EXISTS $tableChoice (
          $_columnId text primary key,
          $_columnParentId text,
          $_columnSelected boolean,
          $_columnIsCorrect boolean,
          // $_columnTestId text,
          $_columnContent text not null)
        ''';

class Choice {
  String id;
  String parentId;
  // String testId;
  bool isCorrect;
  bool selected = false;
  String content;
  Choice({this.id, this.parentId, this.content, this.isCorrect});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      _columnContent: content,
      _columnIsCorrect: isCorrect ? 1 : 0,
      _columnSelected: selected ? 1 : 0,
      _columnParentId: parentId
    };
    if (id != null) {
      map[_columnId] = id;
    }
    return map;
  }

  Choice.fromMap(Map<String, dynamic> map, {int questionId}) {
    content = map[_columnContent];
    id = map[_columnId];
    parentId = map[_columnParentId] ?? "";
    // testId = map[_columnTestId] ?? "";
    isCorrect = (map[_columnIsCorrect] == 1) ? true : false;
    selected = (map[_columnSelected] == 1) ? true : false;
  }
  Choice.copyWith(Choice clone) {
    id = clone.id;
    isCorrect = clone.isCorrect;
    parentId = clone.parentId;
    content = clone.content;
  }

  Choice.cloneWrongChoice(Choice clone) {
    id = Uuid().v1();
    isCorrect = false;
    parentId = clone.parentId;
    content = clone.content;
    selected = false;
  }

  @override
  String toString() {
    return "choice: { id: $id, parentId: $parentId, isCorrect $isCorrect, content: $content }";
  }
  reset() {
    selected = false;
  }
}